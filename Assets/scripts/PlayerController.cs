﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	private Vector2 pos;
	private bool moving = false;
	private GameObject[] npcObjects;
	private NPCController[] npcControlles;

	void Start () {
		//Armazena nossa posiçao inicial
		pos = transform.position;
		npcObjects = GameObject.FindGameObjectsWithTag("npc");
		npcControlles = new NPCController[npcObjects.Length];
		for (int i = 0; i < npcObjects.Length; i++) {
			npcControlles[i] = (NPCController) npcObjects[i].GetComponent(typeof(NPCController));
		}
	}
	
	void Update () {
		checkInput ();

		if (moving) {
			transform.position = pos;
			moving = false;
		}
	}

	private void checkInput() {
		if (Input.GetKeyDown (KeyCode.D) || Input.GetKeyDown (KeyCode.RightArrow)) {
			pos += Vector2.right;
			moving = true;
			ExecuteNPCAI();
		} else if (Input.GetKeyDown (KeyCode.A) || Input.GetKeyDown (KeyCode.LeftArrow)) {
			pos -= Vector2.right;
			moving = true;
			ExecuteNPCAI();
		} else if (Input.GetKeyDown (KeyCode.W) || Input.GetKeyDown (KeyCode.UpArrow)) {
			pos += Vector2.up;
			moving = true;
			ExecuteNPCAI();
		} else if (Input.GetKeyDown (KeyCode.S) || Input.GetKeyDown (KeyCode.DownArrow)) {
			pos -= Vector2.up;
			moving = true;
			ExecuteNPCAI();
		}
	}

	private void ExecuteNPCAI() {
		foreach (NPCController npc in npcControlles) {
			npc.ExecuteAI();
		}
	}

}
