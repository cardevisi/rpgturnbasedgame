﻿using UnityEngine;
using System.Collections;

public class CamController : MonoBehaviour {

	public GameObject target;
	private Vector3 offset;

	void Start () {
		offset = new Vector3 (0f, 0f, -10f);
		Camera.main.orthographicSize = Screen.height / 2 * 1 / 32;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void LateUpdate () {
		transform.position = target.transform.position + offset;
	}
}
